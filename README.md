# README #

cNN :
-------------------

réseau utilisé : 

* 1 couche convolutionelle 3*3
* 1 couche convolutionelle 3*3
* 1 couche max pooling 2*2
* 1 couche convolutionelle 3*3
* 1 couche convolutionelle 3*3
* 1 couche max pooling 2*2

* 1 couche dense ...->512
* 1 couche dense 512 -> 10

La fonction d'activation utilisée est relu.

Le réseau entraîné obtient un score de 84.45% sur l'ensemble de test.



Fichiers :
--------------------------


Les fichiers contiennent les vecteurs correspondant aux activations de l'avant dernière couche (de taille 512 donc).

Chaque fichier contient 10000 lignes pour 10000 images traitées.

** Vecteurs **

Il y a 5 fichiers d'entrainement (trainMaud-{1-5}) et un fichier de test (testMaud).

Chaque ligne contient 512 nombres séparés par un espace

** Labels **

Il y a 5 fichiers d'entrainement (trainMaud-{1-5}-label) et un fichier de test (testMaud-label).

Chaque ligne contient un numéro de classe entre 0 et 9.